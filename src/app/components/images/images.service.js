export class ImagesService {
  constructor ($log, $http) {
    'ngInject';

    this.$log = $log;
    this.$http = $http;
    this.apiHost = 'http://localhost:3002/api/v1/images';
  }

  getImages() {
      return this.$http.get(this.apiHost)
      .then( (response) => {
          return response.data;
      })
      .catch( (error) => {
        this.$log.error('Failed to retrieve API.\n' + angular.toJson(error.data, true));
      });
  }
}

export class MainController {
  constructor ($log, $timeout, imageService) {
    'ngInject';

    this.images = [];
    this.classAnimation = '';
    this.creationDate = 1449722497671;
    this.$log = $log;

    this.activate($timeout, imageService);
  }

  activate($timeout, imageService) {
    this.getImages(imageService);
    $timeout(() => {
      this.classAnimation = 'rubberBand';
    }, 4000);
  }

  getImages(imageService) {
      imageService.getImages().then( (images) => {
          this.$log.info(images[0]);
          this.images = images;
      });
  }

}

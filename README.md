This frontend was initially generated with Yeoman.
In hindsight it took a little longer to get used to it so it may not have been the smartest, but I definitely wanted to use Babel.
I think its the #1 most important thing for a new JS project and I haven't set one of these up in a while.

#Running the client
The client expects the server to be running on localhost:3002


* Just run `gulp serve`
* Navigate to localhost:3000

